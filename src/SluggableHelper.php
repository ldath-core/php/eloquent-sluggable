<?php

namespace Thunderwolf\EloquentSluggable;

class SluggableHelper
{
    /**
     * The name of default slug column.
     */
    const SlugColumn = 'slug';

    /**
     * strings between { and } are used as $key with getAttribute($key) function to take data to slugify.
     * you can use https://laravel.com/docs/8.x/eloquent-mutators to gain even more power
     * by default we are using title colum which must be defined to make this behavior work with default slug pattern
     */
    const SlugPattern = '{title}';

    const ReplacePattern = '/[^\w\/]+/u';

    const Replacement = '-';

    const Separator = '/';

    const Permanent = false;

    /**
     * When using Alpine Images or install intl or this must be set to False
     * due to https://github.com/docker-library/php/issues/1121
     */
    const Transliterate = true;

    /**
     * Parsing columns given to
     *
     * @param array $columns
     * @return array
     */
    public static function parseColumns(array $columns): array
    {
        if (!array_key_exists('slug_column', $columns)) {
            $columns['slug_column'] = self::SlugColumn;
        }
        if (!array_key_exists('slug_pattern', $columns)) {
            $columns['slug_pattern'] = self::SlugPattern;
        }
        if (!array_key_exists('replace_pattern', $columns)) {
            $columns['replace_pattern'] = self::ReplacePattern;
        }
        if (!array_key_exists('replacement', $columns)) {
            $columns['replacement'] = self::Replacement;
        }
        if (!array_key_exists('separator', $columns)) {
            $columns['separator'] = self::Separator;
        }
        if (!array_key_exists('permanent', $columns)) {
            $columns['permanent'] = self::Permanent;
        }
        if (!array_key_exists('transliterate', $columns)) {
            $columns['transliterate'] = self::Transliterate;
        }
        return $columns;
    }

    /**
     * Returns array with columns which should be used when creating Slug
     *
     * @param string $slugPattern
     * @return array
     * @throws SluggableException
     */
    public static function parseSlugPattern(string $slugPattern): array
    {
        $matches = [];
        if(preg_match_all('/{+(.*?)}/', $slugPattern, $matches)) {
            return $matches[1];
        } else {
            throw new SluggableException('Bad or empty Pattern: '.$slugPattern);
        }
    }

    public static function generateRawSlug(
        string $source, string $replace_pattern, string $replacement, bool $should_transliterate
    ): string
    {
        // transliterate
        if ($should_transliterate) {
            $transliterate = self::transliterate($source);
        } else {
            $transliterate = $source;
        }

        // lowercase
        $lowercase = self::lowercase($transliterate);

        // remove accents resulting from OSX's iconv
        $clean = str_replace(array('\'', '`', '^'), '', $lowercase);

        // replace non letter or digits with separator
        $slug = preg_replace($replace_pattern, $replacement, $clean);

        // trim
        $slugTrimmed = trim($slug, $replacement);

        if (empty($slugTrimmed)) {
            return 'n-a';
        }

        return $slugTrimmed;
    }

    /**
     * When using Alpine Images or install intl or this must be set to False
     * due to https://github.com/docker-library/php/issues/1121
     *
     * @param string $source
     * @return string
     */
    protected static function transliterate(string $source): string
    {
        if (extension_loaded('intl')) {
            $source = str_replace(['€'], ['EUR'], $source);
            return transliterator_transliterate('Any-Latin; Latin-ASCII;', $source);
        }
//        if (function_exists('iconv')) {
//            setlocale(LC_ALL, 'pl_PL');
//            return iconv("UTF-8", "ASCII//TRANSLIT", $source);
//        }
        return SimpleTransliteration::convert($source);
    }

    protected static function lowercase(string $source): string
    {
        if (function_exists('mb_strtolower')) {
            $source = mb_strtolower($source);
        } else {
            $source = strtolower($source);
        }
        return $source;
    }
}