<?php

namespace Thunderwolf\EloquentSluggable;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class SluggableServiceProvider extends ServiceProvider
{
    public function register()
    {
        Blueprint::macro('createSluggable', function (array $columns) {
            SluggableBlueprint::createSluggableColumn($this, $columns);
        });

        Blueprint::macro('dropSluggable', function (array $columns) {
            SluggableBlueprint::dropSluggableColumns($this, $columns);
        });
    }
}