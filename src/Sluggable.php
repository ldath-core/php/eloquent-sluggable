<?php

namespace Thunderwolf\EloquentSluggable;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait Sluggable
{
    /**
     * Generated Model configuration cache
     *
     * @var array
     */
    public array $sluggableConfiguration = [];

    /**
     * Configuration for the Sluggable Trait
     *
     * @return array Array in the format ['source' => 'title', 'target' => 'slug'] where keys are configuration
     */
    abstract public function sluggable(): array;

    /**
     * Booting Eloquent Sluggable Trait
     *
     *  creating and created: sent before and after records have been created.
     *  updating and updated: sent before and after records are updated.
     *  saving and saved: sent before and after records are saved (i.e created or updated).
     *  deleting and deleted: sent before and after records are deleted or soft-deleted.
     *  restoring and restored: sent before and after soft-deleted records are restored.
     *  retrieved: sent after records have been retrieved.
     *
     * @return void
     */
    public static function bootSluggable()
    {
        static::saving(function ($model) {
            $target = $model->getSlugColumnName();
            if (!$model->isPermanent() || empty($model->getAttribute($target))) {
                $model->setAttribute($target, $model->createSlug());
            }
        });
    }

    /**
     * Process configuration only once per object
     *
     * @return array
     * @throws SluggableException
     */
    public function getSluggableConfiguration(): array
    {
        if (empty($this->sluggableConfiguration)) {
            $this->sluggableConfiguration = SluggableHelper::parseColumns(get_called_class()::sluggable());
        }
        return $this->sluggableConfiguration;
    }

    // accessing methods

    /**
     * Get the slug column key name.
     *
     * @return string
     * @throws SluggableException
     */
    public function getSlugColumnName(): string
    {
        return $this->getSluggableConfiguration()['slug_column'];
    }

    public function getSlug(): string
    {
        return $this->getAttribute($this->getSlugColumnName());
    }

    /**
     * Get Pattern which will be used to create slug
     *
     * @return string
     * @throws SluggableException
     */
    public function getSlugPattern(): string
    {
        return $this->getSluggableConfiguration()['slug_pattern'];
    }

    /**
     * @return string
     * @throws SluggableException
     */
    public function getReplacePattern(): string
    {
        return $this->getSluggableConfiguration()['replace_pattern'];
    }

    /**
     * @return string
     * @throws SluggableException
     */
    public function getReplacement(): string
    {
        return $this->getSluggableConfiguration()['replacement'];
    }

    /**
     * @return string
     * @throws SluggableException
     */
    public function getSeparator(): string
    {
        return $this->getSluggableConfiguration()['separator'];
    }

    /**
     * @return bool
     * @throws SluggableException
     */
    public function isPermanent(): bool
    {
        return $this->getSluggableConfiguration()['permanent'];
    }

    /**
     * @return bool
     * @throws SluggableException
     */
    public function shouldTransliterate(): bool
    {
        return $this->getSluggableConfiguration()['transliterate'];
    }

    /**
     * @return Builder|Model|object|null
     */
    public static function findOneBySlug(string $slug)
    {
        return (new static)->queryBySlug($slug)->first();
    }

    /**
     * @param string $slug
     * @return Builder
     * @throws SluggableException
     */
    public function queryBySlug(string $slug): Builder
    {
        return $this->newQuery()->where($this->getSlugColumnName(), $slug);
    }

    // sluggable behavior

    /**
     * Create a unique slug based on the object
     *
     * @return string The object slug
     * @throws SluggableException
     */
    protected function createSlug(): string
    {
        $slug = $this->createRawSlug($this->getSlugPattern());
        $slug = $this->limitSlugSize($slug);
        return $this->makeSlugUnique($slug, $this->getSeparator());
    }

    /**
     * Create the slug from the appropriate columns
     *
     * @param string $slugPattern
     * @return string
     * @throws SluggableException
     */
    protected function createRawSlug(string $slugPattern): string
    {
        $attributes = SluggableHelper::parseSlugPattern($slugPattern);
        $outputArray = [];
        foreach ($attributes as $attribute) {
            $outputArray['{'.$attribute.'}'] = $this->cleanupSlugPart($this->getAttribute($attribute));
        }
        return str_replace(array_keys($outputArray), array_values($outputArray), $slugPattern);
    }

    /**
     * Cleanup a string to make a slug of it
     * Removes special characters, replaces blanks with a separator, and trim it
     *
     * @param string $source the text to slugify
     * @return string the slugified text
     * @throws SluggableException
     */
    protected function cleanupSlugPart(string $source): string
    {
        return SluggableHelper::generateRawSlug(
            $source, $this->getReplacePattern(), $this->getReplacement(), $this->shouldTransliterate()
        );
    }


    /**
     * Make sure the slug is short enough to accommodate the column size
     *
     * @param    string $slug                   the slug to check
     * @param    int    $incrementReservedSpace the number of characters to keep empty
     *
     * @return string                            the truncated slug
     */
    protected function limitSlugSize(string $slug, int $incrementReservedSpace = 3): string
    {
        // check length, as suffix could put it over maximum
        if (strlen($slug) > (255 - $incrementReservedSpace)) {
            $slug = substr($slug, 0, 255 - $incrementReservedSpace);
        }

        return $slug;
    }

    /**
     * Get the slug, ensuring its uniqueness
     *
     * @param string $slug the slug to check
     * @param string $separator the separator used by slug
     * @param bool $alreadyExists false for the first try, true for the second, and take the high count + 1
     * @return string the unique slug
     * @throws SluggableException
     */
    protected function makeSlugUnique(string $slug, string $separator, bool $alreadyExists = false): string
    {
        if (!$alreadyExists) {
            $slug2 = $slug;
        } else {
            $slug2 = $slug . $separator;
        }

        $query = get_class($this)::query()
            ->where($this->getSlugColumnName(), $alreadyExists ? 'REGEXP' : '=', $alreadyExists ? '^' . $slug2 . '[0-9]+$' : $slug2);
        if (!is_null($this->getKey())) {
            $query = $query->where($this->getKeyName(), '<>', $this->getKey());
        }

        if (!$alreadyExists) {
            $count = $query->count();
//            var_dump($count);
//            var_dump($this->queryBySlug($slug)->count());
//            var_dump(get_class($this)::getConnection()->getQueryLog());
            if ($count > 0) {
                return $this->makeSlugUnique($slug, $separator, true);
            }

            return $slug2;
        }

        // Already exists
        $object = $query
            ->orderByRaw('LENGTH(?) DESC', [$this->getSlugColumnName()])
            ->orderByDesc($this->getSlugColumnName())
            ->first();

        // First duplicate slug
        if (null == $object) {
            return $slug2 . '1';
        }

        $slugNum = substr($object->getSlug(), strlen($slug) + 1);
        if ('0' === $slugNum[0]) {
            $slugNum[0] = 1;
        }

        return $slug2 . ($slugNum + 1);
    }
}