<?php

namespace Thunderwolf\EloquentSluggable;

use Illuminate\Database\Schema\Blueprint;

class SluggableBlueprint
{
    /**
     * Add sluggable column to the table. Also create an index.
     *
     * @param Blueprint $table
     * @param array $columns
     */
    public static function createSluggableColumn(Blueprint $table, array $columns)
    {
        $columns = SluggableHelper::parseColumns($columns);
        $table->string($columns['slug_column'])->index();
    }

    /**
     * Drop sluggable column and index.
     *
     * @param Blueprint $table
     * @param array $columns
     */
    public static function dropSluggableColumns(Blueprint $table, array $columns)
    {
        $columns = SluggableHelper::parseColumns($columns);
        $table->dropIndex($columns['slug_column']);
        $table->dropColumn($columns['slug_column']);
    }
}