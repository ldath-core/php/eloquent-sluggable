<?php

use Illuminate\Database\Schema\Blueprint;
use PHPUnit\Framework\TestCase;
use Illuminate\Database\Capsule\Manager;
use Thunderwolf\EloquentSluggable\SluggableException;
use Thunderwolf\EloquentSluggable\SluggableHelper;

class SluggableTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $schema = Manager::schema();

        $schema->dropIfExists('articles');
        $schema->dropIfExists('newsletters');

        Manager::connection()->disableQueryLog();

        $schema->create('articles', function (Blueprint $table1) {
            $table1->increments('id');
            $table1->string('title');
            $table1->string('section');
            $table1->createSluggable(['slug_column' => 'link']);
        });

        $schema->create('newsletters', function (Blueprint $table2) {
            $table2->increments('id');
            $table2->string('title');
            $table2->createSluggable([]);
        });

        Manager::connection()->enableQueryLog();
    }

    public function setUp(): void
    {
        $articles = include __DIR__.'/data/articles.php';
        Manager::table('articles')->insert($articles);

        $newsletters = include __DIR__ . '/data/newsletters.php';
        Manager::table('newsletters')->insert($newsletters);

        Manager::connection()->flushQueryLog();

        date_default_timezone_set('Europe/Warsaw');
    }

    public function tearDown(): void
    {
        Manager::table('articles')->truncate();
        Manager::table('newsletters')->truncate();
    }

    public function slugDataProvider(): array
    {
        return [
            'normal title' => ['Test Title One', 'test-title-one'],
            'dash used' => ['Test Title - 1 - From CI', 'test-title-1-from-ci'],
            'transliterate' => ["This is the Euro symbol '€'", "this-is-the-euro-symbol-eur"],
            'Polish' => ['ęóąśłżźćń ĘÓĄŚŁŻŹĆŃ', 'eoaslzzcn-eoaslzzcn']
        ];
    }

    public function slugPatternProvider(): array
    {
        return [
            'default' => ['{title}', ['title']],
            'double' => ['{title}x{section}', ['title', 'section']]
        ];
    }

    /**
     * @dataProvider slugDataProvider
     */
    public function testGenerateSlug(string $source, string $expected): void
    {
        $this->assertEquals(
            $expected, SluggableHelper::generateRawSlug($source, '/[^\w\/]+/u', '-', true)
        );
    }

    /**
     * @dataProvider slugPatternProvider
     * @throws SluggableException
     */
    public function testParseSlugPattern(string $source, array $expected) {
        $this->assertEquals(
            $expected, SluggableHelper::parseSlugPattern($source)
        );
    }

    /**
     * @depends testGenerateSlug
     * @depends testParseSlugPattern
     */
    public function testArticlesRecordOne() {
        $article = Article::query()->find(1);
        $article->setAttribute('title', "Test Title - 1 - From CI");
        $article->save();

        $this->assertEquals('test-title-1-from-ci--a', $article->getAttribute('link'));
    }

    /**
     * @depends testArticlesRecordOne
     */
    public function testGetBySlugRecOne() {
        $article = Article::findOneBySlug('test-title-1');
        $this->assertEquals('Test Title 1', $article->getAttribute('title'));
    }

    public function testGetBySlugRecTwo() {
        $article = Article::findOneBySlug('test-title-2');
        $this->assertEquals('Test Title 2', $article->getAttribute('title'));
    }

    public function testCreateMultipleSameTitles() {
        $article = new Article();
        $article->setAttribute('title', 'My Article');
        $article->setAttribute('section', 'C');
        $article->save();

        $this->assertEquals('my-article--c', $article->getAttribute('link'));

        $article = Article::findOneBySlug('my-article--c');
        $this->assertEquals('my-article--c', $article->getAttribute('link'));

        $article = new Article();
        $article->setAttribute('title', 'My Article');
        $article->setAttribute('section', 'C');
        $article->save();

        $this->assertEquals('my-article--c/1', $article->getAttribute('link'));

        $article = new Article();
        $article->setAttribute('title', 'My Article');
        $article->setAttribute('section', 'C');
        $article->save();

        $this->assertEquals('my-article--c/2', $article->getAttribute('link'));
    }

    public function testNewslettersRecordOne() {
        $newsletter = Newsletter::query()->find(1);
        $this->assertEquals('test-title', $newsletter->getAttribute('slug'));

        $newsletter->setAttribute('title', 'Do not change my slug ;)');
        $newsletter->save();
        $this->assertEquals('test-title', $newsletter->getAttribute('slug'));
    }

    public function testCreateNewsletterRecordTwo() {
        $newsletter = new Newsletter();
        $newsletter->setAttribute('title', 'Uważny Główny Księgowy');
        $newsletter->save();

        $this->assertEquals('uwazny-glowny-ksiegowy', $newsletter->getAttribute('slug'));
    }
}