<?php

return [
    ['id' => 1, 'title' => 'Test Title 1', 'section' => 'A', 'link' => 'test-title-1'],
    ['id' => 2, 'title' => 'Test Title 2', 'section' => 'B', 'link' => 'test-title-2']
];