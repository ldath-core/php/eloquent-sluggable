<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\SQLiteConnection;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Thunderwolf\EloquentSluggable\SluggableServiceProvider;

include __DIR__.'/../vendor/autoload.php';

$container = new Container();
$provider = new SluggableServiceProvider($container);
$provider->register();

$capsule = new Manager($container);
$capsule->addConnection([ 'driver' => 'sqlite', 'database' => ':memory:', 'prefix' => 'prfx_' ]);
$capsule->setEventDispatcher(new Dispatcher);
$capsule->setAsGlobal();
$capsule->bootEloquent();

if ($capsule->getConnection() instanceof SQLiteConnection) {
    $capsule->getConnection()->getPdo()->sqliteCreateFunction('REGEXP', function ($pattern, $value) {
        mb_regex_encoding('UTF-8');
        return (false !== mb_ereg($pattern, $value)) ? 1 : 0;
    });
}

include __DIR__.'/models/Article.php';
include __DIR__.'/models/Newsletter.php';
