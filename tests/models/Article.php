<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentSluggable\Sluggable;

class Article extends Model {

    use Sluggable; // Attach the Sluggable trait to the model

    protected $fillable = ['title', 'section'];

    public $timestamps = false;

    public function sluggable(): array
    {
        return [
            'slug_column' => 'link',
            'slug_pattern' => '{title}--{section}'
        ];
    }
}
