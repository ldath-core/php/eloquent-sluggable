<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentSluggable\Sluggable;

class Newsletter extends Model {

    use Sluggable; // Attach the Sluggable trait to the model

    protected $fillable = ['title'];

    public $timestamps = false;

    /**
     * Example own attribute (accessor)
     *
     * @return string
     */
    public function getPolishFixAttribute(): string
    {
        $source = $this->getAttribute('title');
        if (function_exists('mb_strtolower')) {
            $source = mb_strtolower($source);
        } else {
            $source = strtolower($source);
        }
        $in_letters = array(
            'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź'
        );
        $out_letters = array(
            'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'
        );
        return str_replace($in_letters, $out_letters, $source);
    }

    public function sluggable(): array
    {
        return [
            'slug_pattern' => '{polish_fix}',
            'permanent' => true,
            'transliterate' => false,
        ];
    }
}
